﻿using UnityEngine;
using System.Collections;

public class MainCameraScript : MonoBehaviour {


	private Transform cameraTransform;
	private Vector3 nextPosition;
	private AudioSource _audioSourceThis;
	private bool _restart = false;

	public float Duration = 5.0f;
	public Transform target;
	public Vector2 Offset;
	public float PosDamp = 4f;
	public float MinY = -2.5f;

	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.GetComponent<Transform> ();
		_audioSourceThis = GetComponent <AudioSource> ();
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_FIRE, PlaySound);
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_END, StopSound);
	}
	
	// Update is called once per frame
	void Update () {
		nextPosition = (Vector2)target.position - Offset;
		nextPosition.y = Mathf.Clamp (nextPosition.y, MinY, Mathf.Infinity);
		nextPosition.z = cameraTransform.position.z;
		cameraTransform.position = nextPosition;

	}

	private void PlaySound (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		_audioSourceThis.Play ();
		_restart = true;
		_audioSourceThis.volume = 1.0f;
	}

	private void StopSound (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		_restart = false;
		StartCoroutine (LowVolume ());
	}

	private IEnumerator LowVolume () {
		float fromValue = 1.0f;
		float value = fromValue;
		float toValue = 0.1f;

		float time = 0;

		while (!_restart && time < 1.0f && value != toValue)
		{
			time += Time.deltaTime / Duration;
			value = Mathf.Lerp(fromValue,toValue,time);
			_audioSourceThis.volume = value;
			yield return null;
		}

	}
}
