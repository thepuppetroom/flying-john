﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(Toolkit))]	

public class HeroScript : MonoBehaviour {

	#region Properties
	public bool IsFly {
		get { return _isFly; }
		private set { _isFly = value; }
	}
	public float LaunchingImpulseRatio {
		get { return _launchingImpulseRatio; }
		set { _launchingImpulseRatio = value; }
	}

	public IToolkit Toolkit{
		get { return _toolkit; }
		set { _toolkit = value; }
	}
	#endregion

	#region Variables
	public Vector3 DefaultScale;
	public Vector3 PositionIntoBarrel;
	public float DistanceRatio = 10f;

	[SerializeField] 
	private float _launchingImpulse;
	private float _launchingImpulseRatio;
	private Transform Barrel;
	private bool _isFly = false;
	private float _startPosition = 0.0f;
	private Rigidbody2D _rigidbodyThis;

	private IToolkit _toolkit;
	private IToolkit _emptyToolkit;

	private Vector2 axisX = new Vector2(0, 1);
	#endregion

	void Awake () {
		_isFly = false;

	}

	void Start () {
		_rigidbodyThis = GetComponent<Rigidbody2D> ();
		_toolkit = GetComponent<Toolkit> ();
		_emptyToolkit = _toolkit;
		RestoreEnergy ();


		GameManager.Player.Energy = _toolkit.Energy;

		Barrel = GameObject.FindGameObjectWithTag ("Barrel").transform;

		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_FIRE, OnFire);
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_END, OnLand);
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_RESTART, OnFlightRestart);
		EventManager.Instance.AddListener (EVENT_TYPE.ENERGY_END, OnEnergyEnded);


	}
	
	// Update is called once per frame
	void Update () {
		if (_isFly) {
			GameManager.Player.RecoverEnergy ();
			HandleInput ();
			AirResistance ();
			FlyControl ();

			if (transform.position.x - _startPosition > GameManager.Player.Range * DistanceRatio) {
				EventManager.Instance.PostNotification (EVENT_TYPE.DISTANCE_PLUS, this);
			}
			float speed = _rigidbodyThis.velocity.magnitude;
			if (speed > GameManager.Player.MaxSpeed) {
				GameManager.Player.MaxSpeed = (int)speed;
			}
		}
	}

	private void HandleInput () {
		if (Input.GetKeyDown (KeyCode.Mouse0) && GameManager.Player.isEnoughEnergy) {
			_toolkit.Use(ref _rigidbodyThis);
		}
	}

	private void AirResistance () {
		Vector2 temp;
		temp = _rigidbodyThis.velocity;
		temp.x -= _toolkit.Resistance * Time.deltaTime;
		temp.x = Mathf.Clamp (temp.x, 0, Mathf.Infinity);
		_rigidbodyThis.velocity = temp;
	}

	private void FlyControl () {
		Vector3 rotation;
		float angle;
		angle = 90 - Vector2.Angle (_rigidbodyThis.velocity, axisX);
		rotation = transform.localEulerAngles;
		rotation.z = angle;
		transform.localEulerAngles = rotation;	
	}

	public void ApplyForce (Vector2 force, ForceMode2D mode) {
		_rigidbodyThis.AddForce (force, mode);
	}

	private void RestoreEnergy () {
		GameManager.Player.Energy = _emptyToolkit.Energy;
		GameManager.Player.CurrentEnergy = _emptyToolkit.Energy.maxValue;
		_toolkit = _emptyToolkit;
	}

	#region Events

	private void OnLand (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		_isFly = false;
	}

	private void Run (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		_isFly = true;
	}

	private void OnFlightRestart (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		transform.parent = Barrel;
		transform.localEulerAngles = new Vector3 ();
		transform.localScale = DefaultScale;
		transform.localPosition = PositionIntoBarrel;

		GameManager.Player.Range = 0;
		RestoreEnergy ();
	}

	private void OnFire (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		Vector2 impulse = new Vector2 ();
		impulse.x = _launchingImpulse * LaunchingImpulseRatio * transform.right.x;
		impulse.y = _launchingImpulse * LaunchingImpulseRatio * transform.right.y;

		_isFly = true;
		_rigidbodyThis.isKinematic = false;

		transform.parent = null;
		ApplyForce(impulse, ForceMode2D.Impulse);

		_startPosition = transform.position.x;

		EventManager.Instance.PostNotification (EVENT_TYPE.FLIGHT_START, this);
	}
		

	private void OnEnergyEnded (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		if (_toolkit.Type != EQUIPMENT_TYPE.Empty) {
			RestoreEnergy ();
		}
	}
		
	#endregion
		
}
