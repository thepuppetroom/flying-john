﻿using System;

public interface IInteractive {
	
    void Interact(ref HeroScript hero);

}

