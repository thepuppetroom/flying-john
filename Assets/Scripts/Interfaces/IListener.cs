﻿using UnityEngine;
using System.Collections;


public interface IListener {
	void OnEvent(EVENT_TYPE eventType, Component sender, Object Param = null);
}


