﻿using System;
using UnityEngine;

public interface IToolkit {	

	EQUIPMENT_TYPE Type {
		get;
		set;
	}

	Energy Energy {
		get;
	}

	float Resistance {
		get;
	}

	void Use(ref Rigidbody2D hero);
}


