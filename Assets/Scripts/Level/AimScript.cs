﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AimScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public Transform Canon;
	public Transform CanonTop;
	public Transform LaunchingImpulse;

	[SerializeField]
	private float TopEulerAngle;
	[SerializeField]
	private float LowEurlerAngle;

	private Vector3 _screenPos;
	private float   _angleOffset;
	private CanvasGroup _impulseCanvasGroup;
	private RectTransform _impulseRectTransform;
	private Animation _impulseAnimation;
	private Image _impulseImage;

	void Start () {
		_impulseCanvasGroup = LaunchingImpulse.GetComponent<CanvasGroup> ();
		_impulseRectTransform = LaunchingImpulse.GetComponent<RectTransform> ();
		_impulseAnimation = LaunchingImpulse.GetComponent<Animation> ();
		_impulseImage = LaunchingImpulse.GetComponent<Image> ();
	}

	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData) {
		_screenPos = Camera.main.WorldToScreenPoint(Canon.position);

		Vector3 v3 = Input.mousePosition - _screenPos;
		float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;
		Canon.localEulerAngles = new Vector3(0,0,angle); 

		UI.Instance.SetCanvasGroupActive (_impulseCanvasGroup, true, 0.1f);

		_impulseAnimation.Play ();
		_impulseRectTransform.position = Camera.main.WorldToScreenPoint(CanonTop.position);
	}

	public void OnDrag (PointerEventData eventData) {
		Vector3 v3 = Input.mousePosition - _screenPos;
		_impulseRectTransform.position = Camera.main.WorldToScreenPoint(CanonTop.position);


		float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;
		angle = Mathf.Clamp (angle, LowEurlerAngle, TopEulerAngle);



		Canon.localEulerAngles = new Vector3(0,0,angle); 
		_impulseRectTransform.localEulerAngles = new Vector3(0,0,angle);  


	}

	void IEndDragHandler.OnEndDrag (PointerEventData eventData) {

		_impulseAnimation.Stop ();
		GameManager.Player.Hero.LaunchingImpulseRatio =  _impulseImage.fillAmount;
		Invoke ("PostNotification", 0.5f);

	}

	private void PostNotification() {
		EventManager.Instance.PostNotification (EVENT_TYPE.FLIGHT_FIRE, this);
		UI.Instance.SetCanvasGroupActive (_impulseCanvasGroup, false, 0.1f);
	}



}
