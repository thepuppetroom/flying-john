﻿using UnityEngine;
using System.Collections;

public class Block: MonoBehaviour {
	
    #region Variables

    private BlockItem currentBlock;
	private BoxCollider2D _boxColliderThis;
    //Blocks Factory script is on parent object;
    private BlocksFactory _blocksFactory;

    #endregion
	
    #region Properties
	
    public BlockItem Next {
		get { 
			return currentBlock.nextBlock; 
		}
		set {
			if ( !value.Equals (null) ) {
				currentBlock.nextBlock = value; 
			}
		}
	}

	public BlockItem Previous {
		get { 
			return currentBlock.previousBlock; 
		}
		set {
			if ( !value.Equals (null) ) {
				currentBlock.previousBlock = value; 
			}
		}
	}

    public BlockItem Current {
        get { 
            return currentBlock;
        }
        set {
            if ( !value.Equals (null) ) {
                currentBlock = value; 
            }
        }
    }

	#endregion

    #region Initialization
    void Awake () {
        Current = new BlockItem ();
        Current.thisBlock = GetComponent<Block> ();
    }

	void Start () {
		_blocksFactory = transform.parent.GetComponent<BlocksFactory> ();
		_boxColliderThis = GetComponent <BoxCollider2D> ();
		_boxColliderThis.size = new Vector2 (_boxColliderThis.size.x, 1024.0f);

	}

    #endregion

    #region Methods
    /// <summary>
    /// Spawn next environment block if player is entering current
    /// </summary>
	void OnTriggerEnter2D (Collider2D collider) {
		if (collider.CompareTag ("Player") && Current.nextBlock == null) {
            Next = SpawnNextBlock ().GetComponent<Block>().Current;
            Next.previousBlock = Current;
		}
	}

    /// <summary>
    /// Spawns the next block of environment.
    /// </summary>
    /// <returns>The next block.</returns>
    Transform SpawnNextBlock () {
        Vector3 nextBlockPosition;
        nextBlockPosition = transform.position;
        float blockWidth = GetComponent<SpriteRenderer> ().sprite.bounds.size.x * transform.lossyScale.x;
        nextBlockPosition.x += blockWidth * (float)_blocksFactory.ForwardBlocksCount;
        Transform instance = Instantiate (_blocksFactory.GetRandomBlock(), nextBlockPosition, Quaternion.identity) as Transform;
        instance.transform.SetParent (transform.parent);
        return instance;
    }
		
    #endregion
}
