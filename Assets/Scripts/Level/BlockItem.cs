﻿using UnityEngine;
using System.Collections;

public class BlockItem  {
    public Block thisBlock;
    public BlockItem nextBlock;
    public BlockItem previousBlock;

}
