﻿using UnityEngine;
using System.Collections;

public  class BlocksFactory: MonoBehaviour {

	public Transform[] Blocks;
	public int ForwardBlocksCount;

	public  Transform GetRandomBlock() {
		int randomBlockNumber = Random.Range (0, Blocks.Length - 1);
		return Blocks [randomBlockNumber];
	}
		
}
