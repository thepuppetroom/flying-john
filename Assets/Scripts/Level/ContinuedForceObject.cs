﻿using UnityEngine;
using System.Collections;

public class ContinuedForceObject : MonoBehaviour, IInteractive {

	public Vector2 Vector;

	private ForceMode2D Mode = ForceMode2D.Force;

	public void Interact(ref HeroScript hero) {
		hero.ApplyForce (Vector, Mode);
		GetComponent<Animator> ().SetTrigger ("Crashed");
	}

	void OnTriggerStay2D (Collider2D collider) {
		if (collider.CompareTag ("Player")) {
			HeroScript hero = collider.GetComponent<HeroScript> ();
			Interact (ref hero);
		}
	}
}
