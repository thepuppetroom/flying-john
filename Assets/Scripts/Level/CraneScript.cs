﻿using UnityEngine;
using System.Collections;

public class CraneScript : MonoBehaviour, IInteractive {

    [SerializeField]
    private float Angle;
    [SerializeField]
    private Transform  hook;
    [SerializeField]
    private Transform staticRope;
    [SerializeField]
    //private float ropeRadius;
    private float duration;
    [SerializeField]
    private float toAngle;
    [SerializeField]
    private Transform dynamicRope;
    [SerializeField]
    private Vector2 additionalForce;


    public void Interact(ref HeroScript hero) {
        float currentHeroAngle = hero.transform.eulerAngles.z;
        if (currentHeroAngle <= Angle) {
            hero.transform.SetParent (staticRope);
            StartCoroutine(Rotate (hero.GetComponent<Rigidbody2D>()));
        } 
        else {
            return ;    
        }
    }

 
    private IEnumerator Rotate(Rigidbody2D hero) {
        Debug.Log ("Rotation");
        float time = 0.0f;
        Vector3 temp = new Vector3();
        while (time < duration)
        {
            time += Time.deltaTime / duration;
            float currentAngle = Mathf.Lerp(0, toAngle, time);
            temp = new Vector3 (0, 0, currentAngle);
            staticRope.transform.eulerAngles = temp;
            yield return null;
        }
       
        dynamicRope.eulerAngles = temp;
        hero.transform.SetParent (null);
        hero.AddForce(additionalForce, ForceMode2D.Force);
        staticRope.gameObject.SetActive (false);
        dynamicRope.gameObject.SetActive (true);
        //staticRope.eulerAngles = new Vector3 ();
        Debug.Log ("EndRotation");

    }


    void OnTriggerEnter2D (Collider2D collider) {
        if (collider.CompareTag ("Player")) {
            HeroScript hero = collider.GetComponent<HeroScript> ();
            Interact (ref hero);
        }
    }



}
