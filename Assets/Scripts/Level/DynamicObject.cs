﻿using UnityEngine;
using System.Collections;

public class DynamicObject : MonoBehaviour, IDynamicObject {


	private float maxDisctanceToHero;
	private Animator animatorThis;

	public PullType objectType;

	// Spawn position on x axis:
	// Object must be on left or on right to hero -so x coordinate is "<" or ">" than zero
	// Y-Coordinate is absolute, X - coordinate is relative to hero
	[SerializeField]
	private Vector2 spawnPosition;

	private bool isSpawned = true;

	void Start () {
		maxDisctanceToHero = Mathf.Abs (spawnPosition.x) + 1; 
		spawnPosition.y = transform.position.y;

		animatorThis = GetComponent<Animator> ();
	}

	private bool isInAccess () {
		if (Mathf.Abs (transform.position.x - GameManager.Player.Hero.transform.position.x) > maxDisctanceToHero) {
			return false;
		}
		return true;
	}

	public void Respawn () {
		Vector3 temp = new Vector3 ();
		temp = transform.position;
		temp.x = GameManager.Player.Hero.transform.position.x + spawnPosition.x;
        temp.y = spawnPosition.y;
		transform.position = temp;
		animatorThis.SetTrigger ("Respawn");
		isSpawned = true;
	}

	// Update is called once per some time
	void Update () {
		if (isSpawned && !isInAccess()) {
			if (objectType == PullType.Air) {
				GameManager.AirPull.Add (this);
			} else {
				GameManager.GroundPull.Add (this);
			}
			isSpawned = false;
		}
	}
}
