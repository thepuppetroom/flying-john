﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Toolkit))]	
[RequireComponent(typeof(BoxCollider2D))]	


public class EquipmentObject : MonoBehaviour, IInteractive {

    private Animator _animatorThis;
    private IToolkit _toolkit;

    void Start () {
        _toolkit = GetComponent<Toolkit> ();
    }

    public void Interact(ref HeroScript hero) {
        if (hero.IsFly) {
			hero.Toolkit = _toolkit;
			GameManager.Player.Energy = _toolkit.Energy;
			GameManager.Player.CurrentEnergy = _toolkit.Energy.maxValue;
        }
    }

    void OnTriggerEnter2D (Collider2D collider) {
        if (collider.CompareTag ("Player")) {
            HeroScript hero = collider.GetComponent<HeroScript> ();
            Interact (ref hero);
        }
    }
}
