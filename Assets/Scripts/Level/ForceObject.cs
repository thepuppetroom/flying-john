﻿using UnityEngine;
using System.Collections;

public class ForceObject : MonoBehaviour, IInteractive {

	public Vector2 Vector;

	private Animator animatorThis;
	private ForceMode2D Mode = ForceMode2D.Impulse;

	private void Start () {
		animatorThis = GetComponent<Animator> ();
	}

	public void Interact(ref HeroScript hero) {
		if (hero.IsFly) {
			hero.ApplyForce (Vector, Mode);
			animatorThis.SetTrigger ("Crashed");
		}
	}

	void OnTriggerEnter2D (Collider2D collider) {
		if (collider.CompareTag ("Player")) {
			HeroScript hero = collider.GetComponent<HeroScript> ();
			Interact (ref hero);
		}
	}
}
