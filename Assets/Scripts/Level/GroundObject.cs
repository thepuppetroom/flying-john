﻿using UnityEngine;
using System.Collections;

public class GroundObject : MonoBehaviour, IInteractive {
	
	public EVENT_TYPE [] EventsToPost;
	private Transform skeleton;

	private void  Start () {
		skeleton = transform.FindChild ("Skeleton");
	}

	public void Interact(ref HeroScript hero) {
		if (hero.IsFly) {
			GetComponent<Animator> ().SetTrigger ("Crashed");
			hero.transform.SetParent (skeleton);
			foreach (EVENT_TYPE _event in EventsToPost) {
				EventManager.Instance.PostNotification (_event, this);
			}
		}
	}

	private void OnTriggerEnter2D (Collider2D collider) {
		if (collider.CompareTag ("Player")) {
			HeroScript hero = collider.GetComponent<HeroScript> ();
			Interact (ref hero);
		}
	}
		
}
