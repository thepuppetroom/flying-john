﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class GroundScript : MonoBehaviour, IInteractive {



	public void Interact(ref HeroScript hero) {
			EventManager.Instance.PostNotification (EVENT_TYPE.FLIGHT_END, this);
	}

	void OnTriggerEnter2D (Collider2D collider) {
		if (collider.CompareTag ("Player")) {
			HeroScript hero = collider.GetComponent<HeroScript> ();
			Interact (ref hero);
		}
	}
}
