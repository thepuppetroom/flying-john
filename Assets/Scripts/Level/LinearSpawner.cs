﻿using UnityEngine;
using System.Collections;

public class LinearSpawner : MonoBehaviour {

//	[SerializeField]
//	private Transform[] Blocks;

	[SerializeField] 
	private int SpawnBlockCount;

	private BlocksFactory blocksFactory;
	private Vector3 _temp;
	private BlockItem _current = null; 

	// Use this for initialization
	void Start () {
		blocksFactory = transform.parent.GetComponent<BlocksFactory> ();
		for (int i = 0; i < SpawnBlockCount; i++) {
			Spawn ();
		}
	}

	public void Spawn () {
		Transform instance = Instantiate (blocksFactory.GetRandomBlock(), transform.position, Quaternion.identity) as Transform;
		instance.SetParent (transform.parent);

		Block block = instance.GetComponent<Block> ();
		if (_current == null) {
			_current = block.Current;
			return ;
		}
		block.Previous = _current;
		_current.nextBlock = block.Current;
		_current = block.Current;


		ToNextPosition ();

		//StartCoroutine (Wait ());
	}

	public void ToNextPosition () {
		float spriteSizeX = GetComponent<SpriteRenderer> ().bounds.size.x;

		_temp = transform.position;
		_temp.x += spriteSizeX;
		transform.position = _temp;
	}

//	IEnumerator Wait () {
//		yield return new WaitForSeconds (WaitTime);
//		Spawn ();
//	}
}
