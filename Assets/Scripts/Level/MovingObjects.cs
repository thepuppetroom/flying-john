﻿using UnityEngine;
using System.Collections;

public class MovingObjects : MonoBehaviour {


	public float Velocity;

	// Update is called once per frame
	void Update () {
		Vector3 temp = transform.position;
		temp.x += Velocity * Time.deltaTime;

		transform.position = temp;
	}
}
