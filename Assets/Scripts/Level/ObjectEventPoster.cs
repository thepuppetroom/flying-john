﻿using UnityEngine;
using System.Collections;

public class ObjectEventPoster : MonoBehaviour {

    public EVENT_TYPE[] EVENTS;

    void OnTriggerEnter2D (Collider2D collider) {
        if (collider.CompareTag ("Player")) {
            foreach (EVENT_TYPE e in EVENTS) {
                EventManager.Instance.PostNotification (e, this);
            }
        }
    }
}
