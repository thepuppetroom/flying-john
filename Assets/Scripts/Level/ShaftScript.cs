﻿using UnityEngine;
using System.Collections;

public class ShaftScript : MonoBehaviour {

	public Transform Canon;
	public float TransmitionRatio;
	public float UpSpeed = 0.1f;
	public float Epsilon = 0.001f;

	private Vector3 screenPos;
	private float   angleOffset;
	private float   lastAngle = 0;


	void OnMouseDown () {
		screenPos = Camera.main.WorldToScreenPoint (transform.position);
		Vector3 v3 = Input.mousePosition - screenPos;
		angleOffset = (Mathf.Atan2(transform.right.y, transform.right.x) - Mathf.Atan2(v3.y, v3.x))  * Mathf.Rad2Deg;
	}

	void OnMouseDrag () {
		Vector3 v3 = Input.mousePosition - screenPos;
		float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;

		transform.eulerAngles = new Vector3(0,0,angle+angleOffset);  

		float deltaAngle = lastAngle - angle;
		Vector3 canonRotation = Canon.eulerAngles;
		if (deltaAngle > Epsilon) {
			canonRotation.z += UpSpeed * Time.deltaTime;
		} 
		else if (deltaAngle < -Epsilon) {
			canonRotation.z -= UpSpeed * Time.deltaTime;
		}
		Canon.localEulerAngles = canonRotation;

		lastAngle = angle;

	}
		
}
