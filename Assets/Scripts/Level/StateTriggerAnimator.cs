﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateTriggerAnimator : MonoBehaviour {

	public EVENT_TYPE[] StateTriggers;

	private Dictionary<EVENT_TYPE, string> stateDict;
	private Animator animatorThis;

	void Start () {
		animatorThis = GetComponent<Animator> ();
		stateDict = new Dictionary<EVENT_TYPE, string> ();
		foreach (EVENT_TYPE _event in StateTriggers) {
			EventManager.Instance.AddListener (_event, ChangeState);
			stateDict.Add (_event, _event.ToString ());
		}

		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_RESTART, ResetStates);
	}


	private void ChangeState (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		animatorThis.SetTrigger (Event_Type.ToString ());
	}

	private void ResetStates (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		
		foreach (EVENT_TYPE state_event in StateTriggers) {
			animatorThis.ResetTrigger (stateDict[state_event]);
		}
		animatorThis.SetTrigger (EVENT_TYPE.FLIGHT_RESTART.ToString());
	}
}
