﻿using UnityEngine;
using System.Collections;

public class Toolkit : MonoBehaviour, IToolkit {

	#region Properties
	public Energy Energy {
		get { return _energy; }
	}

	public float Resistance {
		get { return _resistance; }
	}
	#endregion

	#region Variables
	[SerializeField]
	private float _rotateAngle = 5.0f;
	[SerializeField]
	private EQUIPMENT_TYPE type;
	[SerializeField]
	private Vector2 _force;
	[SerializeField]
	private ForceMode2D _forceMode;
	[SerializeField]
	private float forceTime;
    [SerializeField]
    private Energy _energy;
    [SerializeField]
    private float _resistance;

	private float _sin = 0.0f;
	private float _cos = 0.0f;

	#endregion

	// Use this for initialization
	void Start () {
		_cos = Mathf.Cos (Mathf.Deg2Rad * _rotateAngle);
		_sin = Mathf.Sin (Mathf.Deg2Rad * _rotateAngle);
	}
		
	public EQUIPMENT_TYPE Type {
		get { return type; }
		set { type = value; }
	}

	public void Use(ref Rigidbody2D rigidbodyHero) {
		Vector2 tempVelocity;
		tempVelocity.x = rigidbodyHero.velocity.x * _cos - rigidbodyHero.velocity.y * _sin;
		tempVelocity.y = rigidbodyHero.velocity.x * _sin + rigidbodyHero.velocity.y * _cos;
		rigidbodyHero.velocity = tempVelocity;
		GameManager.Player.TakeEnergy ();
		rigidbodyHero.AddForce (_force, _forceMode);
		StartCoroutine (ApplyImpulse(rigidbodyHero));
	}

	private IEnumerator ApplyImpulse (Rigidbody2D rigidbodyHero) {
		float time = 0;
		while (time < forceTime) {
			time += Time.deltaTime;
			rigidbodyHero.AddForce (_force, _forceMode);
			yield return null;
		}
	}

}
