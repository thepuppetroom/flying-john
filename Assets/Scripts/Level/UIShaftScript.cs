﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UIShaftScript : MonoBehaviour, IBeginDragHandler, IDragHandler  {


	public Transform Canon;
	public float TransmitionRatio = 1.0f;
	public float UpSpeed = 10.0f;
	public float Epsilon = 1.0f;

	private Vector3 screenPos;
	private float   angleOffset;
	private float   lastAngle = 0;


	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData) {
		EventManager.Instance.PostNotification (EVENT_TYPE.CIRCLE_ROTATION_START, this);
		screenPos = transform.position;
		Vector3 v3 = Input.mousePosition - screenPos;
		angleOffset = (Mathf.Atan2(transform.right.y, transform.right.x) - Mathf.Atan2(v3.y, v3.x))  * Mathf.Rad2Deg;
	}

	public void OnDrag (PointerEventData eventData) {
		Vector3 v3 = Input.mousePosition - screenPos;
		float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;

		transform.eulerAngles = new Vector3(0,0,angle+angleOffset);  

		float deltaAngle = lastAngle - angle;
		Vector3 canonRotation = Canon.eulerAngles;
		if (deltaAngle > Epsilon) {
			canonRotation.z += UpSpeed * Time.deltaTime;
		} 
		else if (deltaAngle < -Epsilon) {
			canonRotation.z -= UpSpeed * Time.deltaTime;
		}
		Canon.localEulerAngles = canonRotation;

		lastAngle = angle;

	}
		
}
