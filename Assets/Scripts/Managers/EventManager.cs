﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour, IGameManager {

	#region Properties
	//----------------------------
	//General access to the instance
	public static EventManager Instance {
		get {return instance; }
		set {}
	}

	public MANAGER_STATUS status { get; private set; }

	#endregion

	#region Variables
	//Singletone of event manager
	private static EventManager instance;

	//On event Delegate
	public delegate void OnEvent(EVENT_TYPE Event_Type, Component Sender, Object Param = null);

	//Array of Listeners
	private Dictionary<EVENT_TYPE, List<OnEvent>> Listeners = new Dictionary<EVENT_TYPE, List<OnEvent>>();
	#endregion

	#region Methods
	void Awake() {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
			SceneManager.sceneLoaded += RemoveRedundancies;
		} 
		else {
			DestroyImmediate (this);
		}
		StartUp ();
	}

	public void StartUp () {
		status = MANAGER_STATUS.STARTED;
	}
	//--------------------------------------------

	///<summary>
	/// Add Listener function
	/// </summary>
	/// <param name="Event_Type">Event is waiting by Listener </param>
	/// <param name="Listener">Object is waiting of Event</param>
	public void AddListener(EVENT_TYPE Event_Type, OnEvent Listener) {
		List<OnEvent> ListenList = null;

		if(Listeners.TryGetValue(Event_Type, out ListenList)) {
			ListenList.Add(Listener);
			return;
		}

		ListenList = new List<OnEvent> ();
		ListenList.Add (Listener);
		Listeners.Add (Event_Type, ListenList);
	}
	//---------------------------------------------------------------

	///<summary>Post events to Listeners</summary>
	/// <param name="Event_Type">Calling event</param>
	/// <param name="Sender">Calling object</param>
	/// <param name="Param"> Paramert(if need) </param>
	public void PostNotification(EVENT_TYPE Event_Type, Component Sender, Object Param = null) {
		List<OnEvent> ListenList = null;

		if(!Listeners.TryGetValue(Event_Type, out ListenList))
			return;

		for (int i = 0; i < ListenList.Count; i++) {
			if (!ListenList [i].Equals (null)) {
				ListenList [i] (Event_Type, Sender, Param);
			}
		}
	}
	//----------------------------------------------------------------

	//Delete all event with Listeners
	public void RemoveEvent(EVENT_TYPE Event_Type) {
		if (Listeners.ContainsKey (Event_Type)) {
			Listeners.Remove (Event_Type);
		}
	}
	//-----------------------------------------------------------------

	//Delete all events without Listeners
	public void RemoveRedundancies (Scene scene, LoadSceneMode mode) {
		//Create new dict
		Dictionary<EVENT_TYPE, List<OnEvent>> tempListeners = new Dictionary<EVENT_TYPE, List<OnEvent>> ();

		foreach (KeyValuePair<EVENT_TYPE, List<OnEvent>> Item in Listeners) {
			for (int i = Item.Value.Count - 1; i >= 0; i--) {
				if (Item.Value [i].Equals (null)) {
					Item.Value.RemoveAt (i);
				}
			}

			if (Item.Value.Count > 0) {
				tempListeners.Add (Item.Key, Item.Value);	
			}
		}

		Listeners = tempListeners;
	}
	//-----------------------------------------------------------------
	#endregion

}
