﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(PlayerManager))]
[RequireComponent(typeof(Saver))]
[RequireComponent(typeof(MapManager))]
[RequireComponent(typeof(EventManager))]
[RequireComponent(typeof(ObjectsPull))]

public class GameManager : MonoBehaviour {

	public static PlayerManager Player { get; private set; }
	public static Saver GameSaver { get; private set; }
	public static MapManager MapCreator{ get; private set; }
	public static ObjectsPull AirPull { get; private set; }
	public static ObjectsPull GroundPull { get; private set; }

	[SerializeField]
	private string _gameScene = "GameScene";

	private List<IGameManager> startSequence;

	void Awake () {
		Player = GetComponent<PlayerManager> ();
		GameSaver = GetComponent<Saver> ();
		MapCreator = GetComponent<MapManager> ();

		ObjectsPull[] Pulls = GetComponents<ObjectsPull> ();
		foreach (ObjectsPull pull in Pulls) {
			if (pull.type == PullType.Air) {
				AirPull = pull;
			} else {
				GroundPull = pull;
			}
		}
			
		startSequence = new List<IGameManager> ();
		startSequence.Add (Player);
		startSequence.Add (MapCreator);
		startSequence.Add (AirPull);
		startSequence.Add (GroundPull);

		StartCoroutine (StartUpManagers ());
	}

	private IEnumerator StartUpManagers () {
		//Then wait important data from disk
		GameSaver.StartUp ();
		while (GameSaver.status != MANAGER_STATUS.STARTED) {
			yield return null;
		}
		//Now we may launch other managers
		foreach (IGameManager manager in startSequence) {
			manager.StartUp ();
		}
		yield return null;

		int numModules = startSequence.Count;
		int numReady = 0;
		while (numReady < numModules) {
			numReady = 0;

			foreach (IGameManager manager in startSequence) {
				if (manager.status == MANAGER_STATUS.STARTED) {
					numReady++;		
				}
			}
			yield return null;
		}
		EventManager.Instance.PostNotification (EVENT_TYPE.GAME_INIT_END, this);
	}

	public void LoadSceneAsync () {
		EventManager.Instance.PostNotification (EVENT_TYPE.UI_LOAD_SCENE, this);
		UnityEngine.SceneManagement.SceneManager.LoadSceneAsync (_gameScene);
	}
}
