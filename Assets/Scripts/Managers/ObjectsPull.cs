﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PullType {
	Air,
	Ground
}

public class ObjectsPull : MonoBehaviour, IGameManager {

	public MANAGER_STATUS status { 
		get;
		private set;
	}

	#region Variables

	public List<IDynamicObject> Pull;
	public PullType type;

	private float launchingTime = 1.0f;
	private float time = 0.0f;
	[SerializeField]
	private Vector2 LaunchingTimeDiaposone;
	#endregion



	public void StartUp () {
		status = MANAGER_STATUS.STARTED;
	}

	void Awake() {
		Pull = new List<IDynamicObject> ();
	}

	void FixedUpdate() {
		if (time >= launchingTime && Pull.Count > 0) {
			
				int rand = Random.Range (0, Pull.Count);
				Pull [rand].Respawn ();
				Pull.RemoveAt (rand);

			time = 0;
			launchingTime = Random.Range (LaunchingTimeDiaposone.x, LaunchingTimeDiaposone.y);
		}
		time += Time.fixedDeltaTime;
	}


	public void Add(IDynamicObject o) {
		Pull.Add (o);
	}

	public void Remove(IDynamicObject o) {
		if (Pull.Contains(o)) {
			Pull.Remove (o);
		}
	}

	//--------------------------------------
}
