﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct Energy {
	public float maxValue;
	public float minValue;
	public float waveCost;
	public float recoverySpeed;
}

public class PlayerManager : MonoBehaviour, IGameManager {
	
	#region Properties
	//--------------------------------
	public float CurrentEnergy {
		get {
			return currentEnergy;
		}
		set {
			currentEnergy = Mathf.Clamp (value, 0, Energy.maxValue);
			if (currentEnergy == 0) {
				EventManager.Instance.PostNotification (EVENT_TYPE.ENERGY_END, this);
			}
		}
	}

	public bool isEnoughEnergy {
		get { 
			if (currentEnergy >= Energy.waveCost) {
				return true;
			} else {
				return false;			
			}
		}
	}

	public HeroScript Hero {
		get { 
			return hero;
		}
		private set {
			hero = value;		
		}
	}

	public MANAGER_STATUS status { 
		get; 
		private set; 
	}


	public int Points {
		get { return points; }
		set { points = value;}
	}

	public int Range {
		get;
		set;
	}

	public int MaxSpeed {
		get;
		set;
	}

	public int Distruction {
		get;
		set;
	}

	public int Currency {
		get;
		set;
	}

	public int BestScore {
		get;
		set;
	}
		
	#endregion

	#region Variables
	public Energy Energy;
	private float currentEnergy;
	private HeroScript hero;
	private int points;
	private Dictionary<EQUIPMENT_TYPE, IToolkit> ToolsDict;
	#endregion

	public void StartUp () {
        if (GameObject.FindGameObjectWithTag ("Player") != null) {
            hero = GameObject.FindGameObjectWithTag ("Player").GetComponent<HeroScript> ();
        }
		//---------------------------------
		status = MANAGER_STATUS.STARTED;
	}

	public void RecoverEnergy () {
		CurrentEnergy = CurrentEnergy + Energy.recoverySpeed * Time.deltaTime;
	}
	public void TakeEnergy () {
		CurrentEnergy = CurrentEnergy - Energy.waveCost;
	}
}
