﻿using System;
using UnityEngine;
using System.Collections;

public class Saver: MonoBehaviour, IGameManager
{
	public MANAGER_STATUS status { get; private set; }


	private SerializableDictionary<string, int> WorldParams;

	public void StartUp () {
		
		status = MANAGER_STATUS.STARTED;
		GameManager.Player.Currency = PlayerPrefs.GetInt (PlayerPrefsKeys.TotalCurrency.ToString ());
		GameManager.Player.BestScore = PlayerPrefs.GetInt (PlayerPrefsKeys.BestScore.ToString ());

	}

	public void Add (string param, int value) {
		
	}


	public void Save () {
	
	}

	public void Load () {
	
	}
}

