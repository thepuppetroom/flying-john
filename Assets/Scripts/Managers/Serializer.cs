﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using UnityEngine.UI;
using UnityEngine;


public class Serializer<T>
{

   
	public string IOFile
	{
		get {
			return ioFile;
		}
        set {
			if (value != null) {
				ioFile = value;
			}
        }
    }
	public string ResourceFile {
		get {
			return resourceFile;
		}
		set {
			if (value != null) {
				resourceFile = value; 
			}
		}
	}

	private string ioFile;
	private string resourceFile;

    //init your serializer with input/output file
	public Serializer(string _ioFile, string _resourceFile)
    {
		ioFile = _ioFile;
    }
		  

    public void Serialize(T item)
    {
        XmlSerializer mySerializer = new XmlSerializer(typeof(T));
        // To write to a file, create a StreamWriter object.
		string oFile = Path.Combine(UnityEngine.Application.persistentDataPath, ioFile);

		FileStream newFileStream = new FileStream(oFile, FileMode.Create, FileAccess.Write);
		StreamWriter myWriter = new StreamWriter(newFileStream);
        mySerializer.Serialize(myWriter, item);
        myWriter.Close();

    }


    public bool Deserialize(ref T item)
    {
		string iFile = Path.Combine(UnityEngine.Application.persistentDataPath, ioFile);
		FileStream myFileStream = null;
		if (File.Exists (iFile)) {
			myFileStream = new FileStream (iFile, FileMode.Open, FileAccess.Read);
			// Call the Deserialize method and cast to the object type.
			XmlSerializer mySerializer = new XmlSerializer(typeof(T));
			item = (T)mySerializer.Deserialize(myFileStream);
			return true;
		}
		return false;
    }

}

