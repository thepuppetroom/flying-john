﻿using UnityEngine;
using System.Collections;

public class AppearFadeUI : MonoBehaviour {


	public EVENT_TYPE AppearEvent;
	public EVENT_TYPE HideEvent;
	public float FadeDuration = 0.5f;
	public float AppearDuration = 0.5f;


	private CanvasGroup canvasGroup;

	void Start () {
		canvasGroup = gameObject.GetComponent<CanvasGroup> ();

		EventManager.Instance.AddListener (AppearEvent, Appear);
		EventManager.Instance.AddListener (HideEvent, Fade);
	}

	private void Fade (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		UI.Instance.SetCanvasGroupActive (canvasGroup, false, FadeDuration);

	}

	private void Appear (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		UI.Instance.SetCanvasGroupActive (canvasGroup, true, AppearDuration);
	}
}
