﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BestScoreScript : MonoBehaviour {

	private Text UIText;

	// Use this for initialization
	void Start () {
		UIText = GetComponent<Text> ();
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_END, ShowBestScore);
	}

	private void ShowBestScore(EVENT_TYPE Event_type, Component sender, Object Param = null) {
		int currentScore = GameManager.Player.Range + GameManager.Player.Distruction + GameManager.Player.MaxSpeed;
		if (currentScore > GameManager.Player.BestScore) {
			GameManager.Player.BestScore = currentScore;
			PlayerPrefs.SetInt (PlayerPrefsKeys.BestScore.ToString (), currentScore);
			PlayerPrefs.Save ();
		}
		UIText.text = GameManager.Player.BestScore.ToString ();
	}
}
