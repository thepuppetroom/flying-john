﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyController : MonoBehaviour {

	public  Sprite JetpackIcon;
	public Sprite AirplaneIcon;
	public Sprite BatteryIcon;


	public Image FillerImage;
	public Image EnergyTypeImage;


	void Start () {
		EventManager.Instance.AddListener (EVENT_TYPE.GET_AIRPLANE, ToolkitChanged);
		EventManager.Instance.AddListener (EVENT_TYPE.GET_JETPACK, ToolkitChanged);
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_RESTART, ToolkitChanged);
		EventManager.Instance.AddListener (EVENT_TYPE.ENERGY_END, ToolkitChanged);
	}

	// Update is called once per frame
	void FixedUpdate () {
		float toFillAmount = GameManager.Player.CurrentEnergy / GameManager.Player.Energy.maxValue;
		FillerImage.fillAmount = Mathf.Lerp (FillerImage.fillAmount, toFillAmount, Time.deltaTime);
	}

	private void ToolkitChanged (EVENT_TYPE Event_type, Component sender, Object Param = null) {
		switch (Event_type) {
		case EVENT_TYPE.GET_AIRPLANE:
			{
				EnergyTypeImage.sprite = AirplaneIcon;
				break;
			}
		case EVENT_TYPE.GET_JETPACK:
			{
				EnergyTypeImage.sprite = JetpackIcon;
				break;
			}
		case EVENT_TYPE.FLIGHT_RESTART:
			{
				EnergyTypeImage.sprite = BatteryIcon;
				break;
			}
		case EVENT_TYPE.ENERGY_END:
			{
				EnergyTypeImage.sprite = BatteryIcon;
				break;
			}
		}
	}
}
