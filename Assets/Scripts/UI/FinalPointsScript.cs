﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalPointsScript : MonoBehaviour {

	public float Duration = 0.3f;
	public int CurrencyRatio = 10;

	private Text UIPointsText;
	private Text UICurrencyText;
	private Text UITotalCurrencyText;
	private int _finalCurrency;
	// Use this for initialization
	void Start () {
		UIPointsText = GameObject.FindGameObjectWithTag("FinalPointsText").GetComponent<Text> ();
		UICurrencyText = GameObject.FindGameObjectWithTag("FinalCurrencyText").GetComponent<Text> ();
		UITotalCurrencyText = GameObject.FindGameObjectWithTag ("TotalCurrencyText").GetComponent <Text> ();

		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_END, FlightEnd);
	}

	private void FlightEnd(EVENT_TYPE Event_type, Component sender, Object Param = null) {
		StartCoroutine (Compute()); 
	}

	private IEnumerator Compute () {
		float fromValue = GameManager.Player.Range + GameManager.Player.Distruction + GameManager.Player.MaxSpeed;
		float value = fromValue;
		float toValue = 0;

		float time = 0;

		while (time < 1.0f && value != toValue)
		{
			time += Time.deltaTime / Duration;
			value = Mathf.Lerp(fromValue,toValue,time);
			_finalCurrency = (int)(fromValue - value) / CurrencyRatio;
			UICurrencyText.text = _finalCurrency.ToString ();
			UIPointsText.text = ((int)value).ToString ();
			yield return null;
		}
		GameManager.Player.Currency += _finalCurrency;
		UITotalCurrencyText.text = GameManager.Player.Currency.ToString ();  
		PlayerPrefs.SetInt (PlayerPrefsKeys.TotalCurrency.ToString (), GameManager.Player.Currency);
		PlayerPrefs.Save ();
	}
}
