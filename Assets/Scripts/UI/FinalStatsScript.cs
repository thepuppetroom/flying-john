﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalStatsScript : MonoBehaviour {


	public string Range = "RangeValueText";
	public string MaxSpeed = "SpeedValueText";
	public string Distruction = "DistructionValueText";

	private Text UIRangeText;
	private Text UIMaxSpeedText;
	private Text UIDistructionText;

	// Use this for initialization
	void Start () {
		UIRangeText = transform.FindChild("Range").FindChild (Range).GetComponent <Text> ();
		UIMaxSpeedText = transform.FindChild("Speed").Find (MaxSpeed).GetComponent <Text> ();
		UIDistructionText = transform.FindChild("Distruction").Find (Distruction).GetComponent <Text> ();
	
		EventManager.Instance.AddListener (EVENT_TYPE.FLIGHT_END, ShowValue);
	}

	private void ShowValue(EVENT_TYPE Event_type, Component sender, Object Param = null) {
		UIRangeText.text = GameManager.Player.Range.ToString ();
		UIMaxSpeedText.text = GameManager.Player.MaxSpeed.ToString ();
		UIDistructionText.text = GameManager.Player.Distruction.ToString ();
	}
}
