﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {

	private float _timeScale;

	void Start () {
		EventManager.Instance.AddListener (EVENT_TYPE.UI_SHOW_SETTINGS, Pause);
		EventManager.Instance.AddListener (EVENT_TYPE.UI_HIDE_SETTINGS, Run);
	}
	
	private void Pause (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		_timeScale = Time.timeScale;
		Time.timeScale = 0;
	}

	private void Run (EVENT_TYPE Event_Type, Component sender, Object Param = null) {
		Time.timeScale = _timeScale;
	}

}
