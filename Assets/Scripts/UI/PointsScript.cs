﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsScript : MonoBehaviour {

	private Text UIText;

	// Use this for initialization
	void Start () {
		UIText = GetComponent<Text> ();
		EventManager.Instance.AddListener (EVENT_TYPE.DISTANCE_PLUS, OnDistancePlus);
	}
	
	private void OnDistancePlus(EVENT_TYPE Event_type, Component sender, Object Param = null) {
		GameManager.Player.Range++;
		UIText.text = GameManager.Player.Range.ToString ();  
	}
		
}
