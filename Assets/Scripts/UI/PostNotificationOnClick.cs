﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PostNotificationOnClick : MonoBehaviour {

	public EVENT_TYPE [] Event_Types;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Button> ().onClick.AddListener (OnClick);
	}
	
	void OnClick () {
		foreach (EVENT_TYPE e in Event_Types) {
			EventManager.Instance.PostNotification (e, this, null);
		}
	}
}
