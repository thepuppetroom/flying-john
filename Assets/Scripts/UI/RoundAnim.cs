﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundAnim : MonoBehaviour {

	public  Vector3 Axis = new Vector3 (250f,250f,0f);

	private Vector3 _point = new Vector3 (0f,0f,0.1f);

	// Update is called once per frame
	void Update () {
		transform.RotateAround(Axis, _point, 30 * Time.deltaTime);
	}
}
