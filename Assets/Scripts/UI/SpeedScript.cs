﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeedScript : MonoBehaviour {

	private Text _text;
	private Rigidbody2D _heroRigidvody2D;

	void Start () {
		_text = GetComponent<Text> ();
		_heroRigidvody2D = GameManager.Player.Hero.GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		_text.text = ((int)_heroRigidvody2D.velocity.magnitude).ToString ();
	}
}
