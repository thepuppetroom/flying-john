﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UI: MonoBehaviour {
	
	#region Properties
	//----------------------------
	//General access to the instance
	public static UI Instance {
		get {return instance; }
		set {}
	}
	#endregion

	#region Variables
	//Singletone of event manager
	private static UI instance;
	#endregion

	#region Methods
	void Awake() {
		if (instance == null) {
			instance = this;
			//DontDestroyOnLoad (gameObject);
		} 
		else {
			//DestroyImmediate (this);
		}
	}
	//--------------------------------------------

	public void SetCanvasGroupActive (CanvasGroup canvasGroup, bool state, float duration) {
		float alpha = (float)Convert.ToInt32 (state);
		canvasGroup.interactable = state;
		canvasGroup.blocksRaycasts = state;
		StartCoroutine (Fade (canvasGroup, alpha, duration));
	}
		
	private IEnumerator Fade(CanvasGroup canvasGroup, float ToAlpha, float duration) {
		float fromAlpha = canvasGroup.alpha;
		float time = 0.0f;

		while (time < 1.0f && canvasGroup.alpha != ToAlpha)
		{
			time += Time.deltaTime / duration;
			float currentAlpha = Mathf.Lerp(fromAlpha,ToAlpha,time);
			canvasGroup.alpha = currentAlpha;
			yield return null;
		}
	}
	#endregion
}
